package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import Beans.UserBeans;

public class UserDao {

  //emailとpasswordに紐づくユーザ情報を取得する
  public UserBeans findByLoginInfo(String email,String password) {
    Connection conn = null;

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM todolist_user WHERE email=? and password=?";

       // SELECT文を実行し、結果を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1,email);
      pStmt.setString(2,password);
      ResultSet rs = pStmt.executeQuery();
      
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String email1 = rs.getString("email");
      String name = rs.getString("name");
      String password1 = rs.getString("password");

      return new UserBeans(id, email1,  name, password1);


    } catch (SQLException e) {
      e.printStackTrace();
      return null;
  } finally {
      // データベース切断
      if (conn != null) {
          try {
              conn.close();
          } catch (SQLException e) {
              e.printStackTrace();
              return null;
          }
      }
  }
}
}

