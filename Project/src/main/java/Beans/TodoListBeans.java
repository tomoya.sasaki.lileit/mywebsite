package Beans;

import java.io.Serializable;

public class TodoListBeans implements Serializable{
    private int id;
    private String title;
    private int userId;
    
    public TodoListBeans() {
}

    /**
     * @param id
     * @param title
     * @param userId
     */
    public TodoListBeans(int id, String title, int userId) {
      super();
      this.id = id;
      this.title = title;
      this.userId = userId;
    }

    public int getId() {
      return id;
    }

    public void setId(int id) {
      this.id = id;
    }

    public String getTitle() {
      return title;
    }

    public void setTitle(String title) {
      this.title = title;
    }

    public int getUserId() {
      return userId;
    }

    public void setUserId(int userId) {
      this.userId = userId;
    }
}