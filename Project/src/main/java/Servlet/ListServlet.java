package Servlet;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Beans.TodoListBeans;
import Beans.UserBeans;
import DAO.TodoListDao;

/**
 * Servlet implementation class ListServlet
 */
@WebServlet("/ListServlet")
public class ListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
      
      //セッションスコープにセットしたユーザ情報の有無確認
      HttpSession session = request.getSession();
      UserBeans user = (UserBeans) session.getAttribute("userInfo");
      
      if (user == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      
      TodoListDao TodoListDao = new TodoListDao();
      List<TodoListBeans> todoList = TodoListDao.findAll(user.getId());
      
      // リクエストスコープにtodoリスト一覧情報とユーザー情報をセット
      request.setAttribute("user", user);
      request.setAttribute("todoList", todoList);

      // リスト一覧画面へ遷移
      RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/list.jsp");
      dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//文字コードを指定
      request.setCharacterEncoding("UTF-8");
      
   // 検索条件取得
      String title = request.getParameter("title");
      
      HttpSession session = request.getSession();
      UserBeans user = (UserBeans) session.getAttribute("userInfo");

      
    //todolist情報を追加登録
      TodoListDao TodoListDao = new TodoListDao();
   // 検索処理
      List<TodoListBeans> todoList = TodoListDao.search(title, user.getId());
      
      request.setAttribute("todoList", todoList);
      
   // ユーザ一覧画面へ
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/list.jsp");
      dispatcher.forward(request, response);

	}

}
