package Servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Beans.UserBeans;
import DAO.TodoListDao;

/**
 * Servlet implementation class CreateServlet
 */
@WebServlet("/CreateServlet")
public class CreateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public CreateServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO Auto-generated method stub
    HttpSession session = request.getSession();
    UserBeans user = (UserBeans) session.getAttribute("userInfo");

    if (user == null) {
      response.sendRedirect("LoginServlet");
      return;
    }
    request.setAttribute("user", user);
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
    dispatcher.forward(request, response);


  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();
    UserBeans user = (UserBeans) session.getAttribute("userInfo");

    String title = request.getParameter("title");
    int userId = user.getId();
    TodoListDao todoDao = new TodoListDao();

    if (title.equals("")) {
      request.setAttribute("errMsg", "入力された内容は正しくありません");
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
      dispatcher.forward(request, response);
      return;
    }

    todoDao.create(title, userId);

    response.sendRedirect("ListServlet");

  }

}
