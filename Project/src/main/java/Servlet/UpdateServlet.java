package Servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Beans.TodoListBeans;
import Beans.UserBeans;
import DAO.TodoListDao;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	  // パラメータを受け取る
      String todoId = request.getParameter("id");
      
      //セッションスコープにセットしたユーザ情報の有無確認
      HttpSession session = request.getSession();
      UserBeans user = (UserBeans) session.getAttribute("userInfo");
    
      if (user == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      // todolistIdを引数にしてそのレコードを取得
      TodoListDao todolistDao = new TodoListDao();
      TodoListBeans todo = todolistDao.todoDetail(todoId);

      request.setAttribute("title", todo.getTitle());
      request.setAttribute("todo", todo);
      //update.jspにフォワードする
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
        dispatcher.forward(request, response);

       }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//文字コードを指定
      request.setCharacterEncoding("UTF-8");
      
   // idと更新されたパラメータを受け取る

      String id = request.getParameter("todo_id");
      String title = request.getParameter("title");
      //String userId = request.getParameter("user_id");

      
      // titleが未入力の場合にエラーを出す
      if (title.equals("")) {
        //エラー表示
        request.setAttribute("title", title);
        request.setAttribute("errMsg", "入力された内容は正しくありません");
        //create.jspをフォワードする
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
        dispatcher.forward(request, response);
        return;
      }


      // 受け取ったIdを引数にしてtodolistの更新を行なう
      TodoListDao todolistDao = new TodoListDao();
      todolistDao.updateTodo(title,id);

   // listServletを経由して一覧画面に戻る
      response.sendRedirect("ListServlet");
    }

	
	}
