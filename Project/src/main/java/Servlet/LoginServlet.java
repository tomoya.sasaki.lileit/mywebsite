package Servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import Beans.UserBeans;
import DAO.UserDao;

/**Servlet implementation class loginservlet*/
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /** @see HttpServlet*/
    public LoginServlet() {
        super();
    }

	/** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)*/
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
	    throws ServletException, IOException {
		
		
		//login.jspをフォワードする
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
        dispatcher.forward(request, response);
        }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
	    throws ServletException, IOException {
		
	    //文字コードを指定
		request.setCharacterEncoding("UTF-8");
		
		//emailとpasswordのリクエストパラメータを受け取る
		String email = request.getParameter("email");
		String password = request.getParameter("password");

		//DAOに接続
        UserDao userDao = new UserDao();
		
		// 受け取ったパスワードを暗号化する
        //String encodestr = passwordEncoder.encordPassword(password);
		
		//リクエストパラメータを引数にしてDAOでログイン判定
        UserBeans user = userDao.findByLoginInfo(email, password);
		
		 //ログイン出来ない場合エラーを表示する
		if(user == null) {
		  request.setAttribute("errMsg", "ログインに失敗しました");
		// 入力したログインIDを画面に表示
	        request.setAttribute("email", email);
		//login.jspに戻る（フォワードする）
		  RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
	        dispatcher.forward(request, response);
	        return;
		}
		//セッションスコープをセットする
		HttpSession session = request.getSession();
        session.setAttribute("userInfo", user);
        
        //一覧画面にリダイレクトする
        response.sendRedirect("ListServlet");
	}

}
