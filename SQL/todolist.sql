﻿CREATE DATABASE todolist DEFAULT CHARACTER SET utf8;

USE todolist;

CREATE TABLE todolist_user (
 id int(10) PRIMARY KEY AUTO_INCREMENT,
 name varchar(20) NOT NULL,
 email varchar(50) UNIQUE NOT NULL,
 password varchar(20) NOT NULL );
 
INSERT INTO todolist_user (name,email,password)
 VALUE ('田中','tanaka@gmail.com','password');
 
CREATE TABLE todolist (
 id int PRIMARY KEY AUTO_INCREMENT,
 title varchar(50) NOT NULL,
 user_id int NOT NULL,
 INDEX user_id_index(user_id),
 FOREIGN KEY fk_user_id(user_id) REFERENCES todolist_user(id));
 
 INSERT INTO  todolist (title,user_id) VALUE ('走る',1);
 
 INSERT INTO todolist (title,user_id) VALUES ('食べる',1);
